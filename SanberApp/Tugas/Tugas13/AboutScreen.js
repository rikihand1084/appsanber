import React from 'react';
import { StyleSheet, 
        Text, 
        View,
        ScrollView,
        Image,
        TextInput,
        Button

    
    } from 'react-native';

const AboutScreen = () => {
  return (
    <ScrollView>
        <view style ={styles.container}>
            <text style ={styles.judul}> Tentang Saya </text>
            <fontAwesome5 name="user-circle"/>
                size ={200} solid
                color="#EFEFEF"
                style={styles.icon}
            <text style={styles.judul}>Riki Handoko</text>
            <text style={styles.kerjaan}>editor</text>
            <view style={styles.kotak}>
                <text style={styles.nama}>Portofolio</text>
                <view>style={styles.kotakdalam}
                <view>
                    
                </view>
                <fontAwesome5 name="gitlab" size={40} style={styles.icon}/>
                <text style={styles.textdalam}>@rikihand1084</text>
                </view>
                <fontAwesome5 name="github" size={40} style={styles.icon}/>
                <text style={styles.textdalam}>@rikihand1084</text>

            </view>

            <view style={styles.kotak}>
                <text style={styles.kotakdalam}>Hubungi Saya</text>
                <view style={styles.kotakdalamver}>
                  <view> style={styles.kotakdalamverhub}
                        <view>
                            <fontAwesome5 name="facebook" size={40} style={styles.icon}/>
                            
                        </view>
                        <view style ={style.textName}>
                            <text style={styles.textdalam}> ricky sadha </text>
                        </view>

  
                </view>

                <view> style={styles.kotakdalamverhub}
                <view>
                    <fontAwesome5 name="instagram" size={40} style={styles.icon}/> 
                    
                </view>
                </view>
                    <text style={styles.textdalam}> @rickysadhatriyudha </text>
                </view>

                <view> style={styles.kotakdalamverhub}
                <view>
                    <fontAwesome5 name="twitter" size={40} style={styles.icon}/> 
                    <text style={styles.textdalam}> @richiesadha</text>
                </view>
              </view>

            </view>
        </view>
    </ScrollView>
  );
}



const styles = StyleSheet.create({

  container: {
    MarginTop : 64
  },

  Judul: {
    fontSize : 36,
    fontweight : 'bold',
    color : '#003366',
    textAlign : 'center',

  },
  icon: {
    textAlign : 'center'
  },

  name: {
    fontSize : 24,
    fontweight : 'bold',
    color : '#003366',
    textAlign : 'center',
  },

  kerjaan: {
    fontSize : 16,
    fontweight : 'bold',
    color : '#3EC6FF',
    textAlign : 'center',
    marginBottom : 7,
    },

  kotak : {
    borderColor : 'blue',
    borderRadius : 10,
    borderBottomColor : '#000',
    padding : 5,
    backgroundColor : '#EFEFEF' ,
    marginBottom : 9,
  },

  kotakdalam : {
    borderTopWidth : 2,
    bordertopColor : '#003366',
    flexDirection : 'row',
    justifyContent : 'space-around',

  },
  kotakdalamver : {
    borderTopWidth : 2,
    bordertopColor : '#003366',
    flexDirection : 'column',
    justifyContent : 'space-around',

  },
  kotakdalamverhub : {
    height : 50,
    flexDirection : 'row',
    justifyContent : 'center',
    marginBottom : 2,

  },
  juduldalam : {
    fontSize : 18,
    color : '#003366',

  },
  textdalam : {
    fontSize : 16,
    fontweight : 'bold',
    color : '#003366',
    textAlign : 'center',

  },
  input : {
    height : 40,
    borderColor : 'grey',
    borderwidth : 1

  },
  textName : {
    justifyContent : 'center',
    marginLeft : 20,
  },

})
    